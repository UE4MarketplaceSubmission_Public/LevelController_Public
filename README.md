



# LevelController2 (Transform Levels At Runtime In Realtime)

"LevelController2": https://www.unrealengine.com/marketplace/en-US/product/levelcontroller2-transform-levels-at-runtime-in-realtime

**NOTE:** the **free version** is "LevelController1"(https://www.unrealengine.com/marketplace/en-US/product/levelcontroller-transform-levels-at-runtime-in-realtime)

1. This "LevelController2" plugin has more features than the "LevelController1" free version: such as transform the levels using a custom origin point(Can be changed every tick at runtime). 

2. And the **performance** of this "LevelController2"  plugin is **165%** of the "LevelController1" free version.
3. "LevelController2" plugin can Tick Actor or Tick LevelScriptActor(Level Blueprint) in Editor or at Runtime by inheriting  from "AExActor" or using the "AExLevelScriptActor" class as custom "LevelScriptActor" Class.

![image-20221225190630875](README/00_Res/01_Images/image-20221225190630875.png)

## Details

### SHORT DESCRIPTION

This UE C++ plugin lets you could transform UE Levels(Maps) at Runtime in Realtime.

### LONG DESCRIPTION 

Preview：[Video Demo of "LevelController (Transform Levels At Runtime In Realtime)" UE Plugin](https://youtu.be/hLIs0Bou7mU)

This plugin makes it possible for you to transform UE Levels (Maps) at Runtime in Realtime.

What you need to do is to call our blueprint functions of our plugin.

**NOTE:** the free version is "LevelController1"(https://www.unrealengine.com/marketplace/en-US/product/levelcontroller-transform-levels-at-runtime-in-realtime)

1. This plugin has more features than the free version: such as transform the levels using a custom origin point(Can be changed every tick at runtime). 

2. And the **performance** of this plugin is **165%** of the free version.
3. Can Tick Actor or Tick LevelScriptActor(Level Blueprint) in Editor or at Runtime by inheriting  from "AExActor" or using the "AExLevelScriptActor" class as custom "LevelScriptActor" Class.



### TECHNICAL INFORMATION

Features: 

- Transform UE Levels(Maps) at Runtime in Realtime.
- Transform the levels using a custom origin point(Can be changed every tick at runtime). 
- The **performance** of this plugin is **150%** of the free version.
- Can Tick Actor or Tick LevelScriptActor(Level Blueprint) in Editor or at Runtime by inheriting  from "AExActor" or using the "AExLevelScriptActor" class as custom "LevelScriptActor" Class.

Code Modules: 

- LevelController (Module Type: Runtime)

Number of Blueprints: 1.

Number of C++ Classes: 4.

Network Replicated: No.

Supported Development Platforms: Windows (x64), Mac.

Supported Target Build Platforms: Windows (x86, x84), Mac, iOS, Android, HTML5.

Documentation: https://gitlab.com/UE4MarketplaceSubmission_Public/LevelController_Public

Example Project: https://gitlab.com/UE4MarketplaceSubmission_Public/LevelController_Public



## Samples:

### Sample01: 

**Sample Level:** /Script/Engine.World'/Game/LevelController2Demo/01_Maps/002_MAP_LevelController2Demo_Overview_02.002_MAP_LevelController2Demo_Overview_02'

![image-20221225202837953](README/00_Res/01_Images/image-20221225202837953.png)

**Moving Level(Change Streaming Method to "Always Loaded"):** /Script/Engine.World'/Game/Maps/SunTemple.SunTemple'

![image-20221225202958565](README/00_Res/01_Images/image-20221225202958565.png)

**"BPA_LevelTransformer":**/Script/Engine.Blueprint'/Game/LevelController2Demo/02_Assets/04_LevelTransformer/BPA_LevelTransformer.BPA_LevelTransformer'

Note: Should set the Moving Level name in the "LevelsToBeTransformed" property of "BPA_LevelTransformer" Actor.

![image-20221225203208876](README/00_Res/01_Images/image-20221225203208876.png)

![image-20221225203243455](README/00_Res/01_Images/image-20221225203243455.png)

## **Notices**:

### Notice01: In UE, if after building the Static Lighting for One Level(Level A), the lighting effects of Level A is correct, but the lighting of Level A is abnormal(Some static meshes in Level A are darkened) if the Level A is rotated through the code or Level "Rotation" attribute in the "Level Details" panel. This issue can be handled as follows:

Method 1: In the materials of the darkened static meshes, set the material's "Use Lightmap Directionality" property to false.

Method 2: For the Mobile side (e.g. iOS), set the "Disable Lightmap directionality" property to true in the "Project Settings->Platforms->iOS Material Quality-Metal->Forward Rendering Overrides" panel.



### Notice02: About Character. If Character is not in the Moving Level but the floor of the character is in the Moving Level. The floor of the character should be Movable("Mobility" property of the floor StaticMesh should be "Movable") otherwise the character will not move with the floor.

 The floor of the transormed Levels should be “Movable”. Otherwise the player Character will not move with the floor.

![image-20221225152349318](README/00_Res/01_Images/image-20221225152349318.png)



### Notice03:  If Character is shaking during Moving with floor, should add this actor(Which will transform levels in its Tick function) as the Tick Prerequistie Actor for the "CharacterMovement" Component of Character.

**Samples:**

"BPA_LevelTransformer": /Script/Engine.Blueprint'/Game/LevelController2Demo/02_Assets/04_LevelTransformer/BPA_LevelTransformer.BPA_LevelTransformer'

![image-20221225155902737](README/00_Res/01_Images/image-20221225155902737.png)



### Notice04: By inheriting from "AExActor" class, you can make your blueprint Actor to  Tick in the Editor or at runtime, allowing you to see the Level transform changing in real time when you adjusting the LevelSequence.

**To Tick in Editor:** the "TickInEditor" property should be true, and use the "Tick(Editor&Runtime)" Tick Event).

![image-20221225160059271](README/00_Res/01_Images/image-20221225160059271.png)

![image-20221225160211722](README/00_Res/01_Images/image-20221225160211722.png)

![image-20221225160243676](README/00_Res/01_Images/image-20221225160243676.png)



### Notice05: These models need to be set to "Stationary" or "Movable" to avoid fuzzy jitter during moving.  Or And disable the "Motion Blur" In “ProjectSettings->Engine->Rendering”. If you do not want to change the Meshes to "Stationary" or "Movable", you can try to change the "Anti-Aliasing Method" to “FXAA” or “MSAA".

Before:

![image-20221225161430501](README/00_Res/01_Images/image-20221225161430501.png)

After: Change the Mesh to "Stationary" or "Movable".

![image-20221225163654129](README/00_Res/01_Images/image-20221225163654129.png)

or "Movable":

![image-20221225161123487](README/00_Res/01_Images/image-20221225161123487.png)

Results:

![image-20221225161527356](README/00_Res/01_Images/image-20221225161527356.png)

If you do not want to change the Meshes to "Stationary" or "Movable", you can try to change the "Anti-Aliasing Method" to “FXAA” or “MSAA".

![image-20221225161947376](README/00_Res/01_Images/image-20221225161947376.png)

### Notice06: For physical actors with physical simulation enabled, in order to get the correct physical representation, they should not be in the moving level, but they can collide correctly with objects in the moving level or moving with the moving level actors by collision.

### Notice07: Functions Overview.

**Note:** For getting maximum performance, the "bool" parameter for "UpdateLevelTransform()" or "UpdateLevelTransformAndOrigin()" needs to be set to true.

![image-20221225203535640](README/00_Res/01_Images/image-20221225203535640.png)